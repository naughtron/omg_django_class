from django.apps import AppConfig


class OmgBlogConfig(AppConfig):
    name = 'omg_blog'
