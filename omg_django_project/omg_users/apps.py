from django.apps import AppConfig


class OmgUsersConfig(AppConfig):
    name = 'omg_users'

    def ready(self):
        import omg_users.signals
