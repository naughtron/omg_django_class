from django.shortcuts import render, redirect
# user create form alread exists in django
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import UserRegisterForm, UserUpdateForm, ProfileUpdateForm

def register(request):
    if request.method == 'POST':
        # print(request.method)
        form = UserRegisterForm(request.POST)
        # print(type(form))
        # print(form.errors)
        # print(form.is_valid())
        if form.is_valid():
            form.save() # save the user on success
            username = form.cleaned_data.get('username')
            # redirect user to login page after create is done.
            messages.success(request, f'Your account has been created! You are now able to log in.')
            return redirect('login')
    else:
        form = UserRegisterForm()
    return render(request, 'omg_users/register.html', {'form': form})

@login_required
def profile(request):
    if request.method == 'POST':
        # created instances of the user/profile update forms
        u_form = UserUpdateForm(request.POST, instance=request.user) # populate with current user info
        p_form = ProfileUpdateForm(request.POST,
                                    request.FILES, 
                                    instance=request.user.profile)
        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            messages.success(request, f'Your account has been updated.')
            return redirect('profile')
    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)

    context = {
        'u_form': u_form,
        'p_form': p_form
    }
    return render(request, 'omg_users/profile.html', context)