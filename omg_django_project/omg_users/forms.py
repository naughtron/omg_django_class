from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Profile

# adding email so it can be used with the registration view
# this UserRegistration form is inheriting from UserCreationForm
class UserRegisterForm(UserCreationForm):
    email = forms.EmailField() # default is required

    # meta is the model you want this form to interact with. 
    # in this case when the form validates it will then create a new user. 
    class Meta:
        model = User # the model that will be effected
        fields = ['username', 'email', 'password1', 'password2']

# create a form that interacts with an existing database model
class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()
    class Meta:
        model = User
        # this will only allow users to update username and email
        fields = ['username', 'email']

class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['image']