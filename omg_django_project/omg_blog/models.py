from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User # bring in the users model this was created during the migrate command / super-user gen
# each class in here is its own table in the database.
from django.urls import reverse


# posts model
class Post(models.Model):
    # attributes / fields in the database
    title = models.CharField(max_length=100)
    content = models.TextField()
    # good use of auto_now_add=True would be for last modified
    date_posted = models.DateTimeField(default=timezone.now) 
    author = models.ForeignKey(User, on_delete=models.CASCADE) # on_delete means if the user is deleted from the system then also delete posts

    # create a dunder str method 'double underscore'
    def __str__(self):
        return self.title

    # find the location to a specific post
    def get_absolute_url(self):
        return reverse('post-detail', kwargs={'pk':self.pk})
