from django.shortcuts import render, get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.models import User
from django.views.generic import (
    ListView, 
    DetailView, 
    CreateView,
    UpdateView,
    DeleteView
)
from .models import Post

def home(request):
    # passing in test data to the view
    context = {
        'posts': Post.objects.all() # query the db for posts
    }
    # render is used to render templates you create.
    # passing in the optional contex data
    return render(request, 'omg_blog/home.html', context)

def about(request):
    # passing in the title to the about template with a small dict
    return render(request, 'omg_blog/about.html', {'title': 'About'})

class PostListView(ListView):
    model = Post
    # change the template that the PostListView is looking for.
    template_name = 'omg_blog/home.html'
    content_object_name = 'posts'
    ordering = ['date_posted'] # set post order so it is newest to oldest
    paginate_by = 5

class UserPostListView(ListView):
    model = Post
    # change the template that the PostListView is looking for.
    template_name = 'omg_blog/user_posts.html'
    content_object_name = 'posts'
    paginate_by = 5

    def get_queryset(self):
        user = get_object_or_404(User, username=self.kwargs.get('username'))
        return Post.objects.filter(author=user).order_by('-date_posted')


class PostDetailView(DetailView):
    model = Post

class PostCreateView(LoginRequiredMixin, CreateView):
    model = Post
    fields = ['title', 'content']

    # override the form valid method
    def form_valid(self, form):
        # the form you are trying to submit, set the author to the current logged in user
        form.instance.author = self.request.user

        return super().form_valid(form)

class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post
    fields = ['title', 'content']

    # override the form valid method
    def form_valid(self, form):
        # the form you are trying to submit, set the author to the current logged in user
        form.instance.author = self.request.user
        return super().form_valid(form)
    
    # verify that the logged in user has the rights to edit the post
    def test_func(self):
        # get the object we are trying to update
        post = self.get_object()
        # check if the author is associated with the post
        if self.request.user == post.author:
            return True
        return False

class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post
    success_url = '/'

    # verify that the logged in user has the rights to delete the post
    def test_func(self):
        # get the object we are trying to update
        post = self.get_object()
        # check if the author is associated with the post
        if self.request.user == post.author:
            return True
        return False




