from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.dispatch import receiver
from .models import Profile

# reciever decorator
# when a user is saved send this signal post_save
# them the signal is recieved by the reciever create_profile
@receiver(post_save, sender=User)
def create_profile(sender, instance, created, **kwargs):
    # if that user is then created
    if created:
        # create a user with the instance that was created
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_profile(sender, instance, **kwargs):
    # instance of User
    instance.profile.save()


