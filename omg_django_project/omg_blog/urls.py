from django.urls import path
from . import views
from .views import PostListView, PostDetailView, PostCreateView, PostUpdateView, PostDeleteView, UserPostListView
from omg_users import views as user_views


# make names unique in the event you want to runa rev lookup
urlpatterns = [
    # replacing views.home with PostListView
    path('', PostListView.as_view(), name='omg-blog-home'),
    path('user/<str:username>', UserPostListView.as_view(), name='user-posts'),
    # using a class based view with vars in the url
    path('post/<int:pk>/', PostDetailView.as_view(), name='post-detail'),
    # create a new post
    path('post/new/', PostCreateView.as_view(), name='post-create'),
    # update a post
    path('post/<int:pk>/update/', PostUpdateView.as_view(), name='post-update'),
    # delete a post
    path('post/<int:pk>/delete/', PostDeleteView.as_view(), name='post-delete'),
    path('about/', views.about, name='omg-blog-about'),
    path('register/', user_views.register, name='omg-register'),
]